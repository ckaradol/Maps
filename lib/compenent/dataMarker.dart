import 'package:flutter/material.dart';

class DataMarker extends StatelessWidget {
  const DataMarker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset("assets/images/motokurye.png"),
    );
  }
}
